//******************************************************************************
//
// Copyright (c) 2016 Microsoft Corporation. All rights reserved.
//
// This code is licensed under the MIT License (MIT).
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//******************************************************************************

import org.junit.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.util.concurrent.TimeUnit;
import java.net.URL;
import io.appium.java_client.windows.WindowsDriver;

public class CalculatorTest {

    private static WindowsDriver<WebElement> CalculatorSession = null;
    private static WebElement CalculatorResult = null;

    @BeforeClass
    public static void setup() {
        try {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
            CalculatorSession = new WindowsDriver<WebElement>(new URL("http://127.0.0.1:4723"), capabilities);
            CalculatorSession.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);

            CalculatorResult = CalculatorSession.findElementByAccessibilityId("CalculatorResults");
            Assert.assertNotNull(CalculatorResult);

        }catch(Exception e){
            e.printStackTrace();
        } finally {
        }
    }

    @Before
    public void Clear()
    {
        CalculatorSession.findElementByAccessibilityId("clearButton").click();
        //Assert.assertEquals("0", _GetCalculatorResultText());
        
    }

    @AfterClass
    public static void TearDown()
    {
        CalculatorResult = null;
        if (CalculatorSession != null) {
            CalculatorSession.quit();
        }
        CalculatorSession = null;
    }

    @Test 
    public void Addition()
    {
        CalculatorSession.findElementByAccessibilityId("num9Button").click();
        CalculatorSession.findElementByAccessibilityId("plusButton").click();
        CalculatorSession.findElementByAccessibilityId("num7Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        Assert.assertEquals("2", _GetCalculatorResultText());
    }

    @Test
    public void Combination()
    {
        CalculatorSession.findElementByAccessibilityId("num7Button").click();
        CalculatorSession.findElementByAccessibilityId("multiplyButton").click();
        CalculatorSession.findElementByAccessibilityId("num9Button").click();
        CalculatorSession.findElementByAccessibilityId("plusButton").click();
        CalculatorSession.findElementByAccessibilityId("num1Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        CalculatorSession.findElementByAccessibilityId("divideButton").click();
        CalculatorSession.findElementByAccessibilityId("num8Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        Assert.assertEquals("8", _GetCalculatorResultText());
    }

    @Test
    public void Division()
    {
        CalculatorSession.findElementByAccessibilityId("num8Button").click();
        CalculatorSession.findElementByAccessibilityId("num8Button").click();
        CalculatorSession.findElementByAccessibilityId("divideButton").click();
        CalculatorSession.findElementByAccessibilityId("num1Button").click();
        CalculatorSession.findElementByAccessibilityId("num1Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        Assert.assertEquals("8", _GetCalculatorResultText());
    }

    @Test
    public void Multiplication()
    {
        CalculatorSession.findElementByAccessibilityId("num9Button").click();
        CalculatorSession.findElementByAccessibilityId("multiplyButton").click();
        CalculatorSession.findElementByAccessibilityId("num9Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        Assert.assertEquals("81", _GetCalculatorResultText());
    }

    @Test
    public void Subtraction()
    {
        CalculatorSession.findElementByAccessibilityId("num9Button").click();
        CalculatorSession.findElementByAccessibilityId("minusButton").click();
        CalculatorSession.findElementByAccessibilityId("num1Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        Assert.assertEquals("8", _GetCalculatorResultText());
    }
    
    @Test
    public void Multiplicationx2()
    {
        CalculatorSession.findElementByAccessibilityId("num6Button").click();
        CalculatorSession.findElementByAccessibilityId("multiplyButton").click();
        CalculatorSession.findElementByAccessibilityId("num7Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        Assert.assertEquals("42", _GetCalculatorResultText());
        
    }
    
    @Test 
    public void Additionx2()
    {
        CalculatorSession.findElementByAccessibilityId("num3Button").click();
        CalculatorSession.findElementByAccessibilityId("plusButton").click();
        CalculatorSession.findElementByAccessibilityId("num7Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        Assert.assertEquals("4", _GetCalculatorResultText());
    }
    @Test
    public void Divisionx2()
    {
        CalculatorSession.findElementByAccessibilityId("num6Button").click();
        CalculatorSession.findElementByAccessibilityId("num6Button").click();
        CalculatorSession.findElementByAccessibilityId("divideButton").click();
        CalculatorSession.findElementByAccessibilityId("num2Button").click();
        CalculatorSession.findElementByAccessibilityId("num2Button").click();
        CalculatorSession.findElementByAccessibilityId("equalButton").click();
        Assert.assertEquals("3", _GetCalculatorResultText());
    }
    

    protected String _GetCalculatorResultText()
    {
        // trim extra text and whitespace off of the display value
        return CalculatorResult.getText().replace("Display is", "").trim();
    }

}
